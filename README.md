# *max7219mod*

# Description

*max7219mod* is a personal project to obtain a GNU/Linux kernel module to drive
an array of MAX7219 controlled led matrices. The rationale behind the use of
a kernel driver instead of existing *usermode* driver (i.e. *spidev*) is this.

- Separation of concerns: I would like the driver to do one only thing (well).
	Not mixing the main driving tasks with font rendering or other functions
	dealing with specific features is paramount.

- Flexibility: I want the driver to be able to deal both with text and with
  bitmap rendering. The text rendering should not be bound (in the driver) to
	any font format. Also, configuration of hardware should be easy (i.e.
	changing a simple parameter when loading the module).

- Asynchronous: I don't want to wait while the scrolling a message takes place.

- Queued: If you want to send several messages in sequence, would you like to
	wait for the completion of the message scrolling before being able to send
	the next message? No, the messages should be queued.

- Compatibility: I want to use existing and standard font formats, not
	handmade, custom converted, etc. There exist good formats and bitmaped 8 bits
	heightfonts available online, i.e. PSF. Also, I choose to use hardware SPI
	capabilities instead of GPIOS or bitbanging for wider hardware compatibility.

- Internationallity: My first concern is that most of existing software (and font)
	support does not support **international languages**. Custom and handmade
	fonts usually didn't bother to deal with accents, money or national symbols.
	Luckyly, some standard fonts and font formats (like PSF) does support
	*latin1* and *unicode* symbols. Why not leverage these?

- Compactness: I want to provide a low level interface (kernel module) and
	also a medium level one (C library) without too much software dependencies
	to begin with. Of course, `python`, `nodejs` or other language bindings
	could be make on top of the library or using the *ioctl* API, but being able
	to compile a user program statically or depending only on base libraries is a
	requirement for embedded or constrained platforms I wanted to meet.

# Architecture

Any user program willing to talk to the kernel module has two choices here:

1. **Low level API**: Use the *chardev* file API (`ioctl, open, close`) to talk
   directly to the kernel module, but take into account that data sent to this
	 device is **not** text to render, but rendered bitmaps to be sent to the
	 MAX7219 controller. Refer to module's source code for details.

2. **High level API**: I provide here an user space interface library and some
   example programs using the library. Dealing with fonts and text is **only**
	 supported by the library. So, if you don't want to bother with font formats
	 or text to font rendering, this is your way to go.

## Kernel module

Currently, these are the parameters that can be specified while loading the
module:

- `bus`: Available SPI bus master number to use (from 0 to 3). Can't be changed
	at runtime. Defaults to 1.

- `array_count`: Number of led matrices chained to the same bus array, from 1
	to 8 (although you can change the limits before compiling if you want). Can't
	be changed at run time. Defaults to 5.

- `intensity`: Initial intensity every matrix in the array. It should be a
	value from 0 to 15, meaning increasing light intensity from 0 (visible) to 15
	(max intensity). Defaults to 1.

- `test_value`: Test value that will be shown in every row of every led matrix
	at load. The value should be between 0 and 255, being a bit pattern (0 means
	all leds off, 255 all leds on). This can be used to test that the module is
	driving the leds correctly when the module is loaded. Defaults to 0.

- `scroll_mode`: Initial scroll mode in the chain. 0 means that the message
	(text or bitmap) will be written with no scroll, 1 to scroll from left to
	right, 2 to scroll from the bottom to the top and 3 top to bottom scrolling.
	This can be changed at runtime with the library function `set_scroll_mode` or
	using a corresponding `ioctl` (see source code). Defaults to 1 (scroll left
	to right). Note that, in *no scroll* or *scroll top/bottom, botton/top* mode,
	if the data sent to the driver (text or bitmap) exceeds the display width,
	then it will be split in fixed chunks of a size equal to the display width
	and displayed in turns.

## The `max7219m` library

This usermode library provides a simple C interface to the kernel module
chardev `ioctl`'s and `open`/`close` system calls that allows the kernel module
to receive data and operation parameter (i.e. `msdelay` and `scroll_mode`).

Also, font file loading and text rendering functions are provided as library
services to simplify user interaction with the kernel module.

Since PSF *gzip* compressed fonts are supported, `libz` is required as a
compilation and runtime dependency of the library and also of the programs
compiled with the library.

All the example programs use this library. Please, refer to the examples source
code for library usage demonstration.

## The `sendtext` program

This program provides a simple command line interface (CLI) to test the kernel
module, but it could be useful for `ssh` or `crontab` based applications.

    # ./sendtext fonts/iso01a-08.psf.gz 1 20 "Hello world!"

This will send the text `Hello world!` to the kernel module queue to be
displayed in *scroll left to right* (1) mode with a delay of 20 ms on every
scroll step.

## The `usendtextd` daemon

A simple web service to demonstrate how remote sending of messages could be
provided with not to much efort. This example uses the excellent
[libmicrohttpd](https://www.gnu.org/software/libmicrohttpd) library to present
and handle a simple form where a message (and options) could be issued to the
kernel module. The library `max7219m` is used to interface the kernel module
where the delay and scroll mode parameters could be specified too.

Since the kernel module enqueues received messages, no temporal requirements
could be met in the service. Only a *successfully enqueued* message will be
notified back indicating that the message will be eventually displayed.

# Hardware requirements (for testing & deployment)

I tested this module with a chain of five matrices, but it could be possible, in
theory, to chain up to 256 modules. If you want to use the software with more
than 8 modules, you'd have to tweak the driver (and recompile it), but only to
widen the max limit which is arbitrarily limited to 8 matrices.

You need an array of one to eight [MAX7219 controlled 8x8 LED matrices](https://www.ebay.com/sch/i.html?_nkw=max7219+dot+matrix+module)
chained together and connected to the SPI master bus of a Raspberry PI, Olimex
Lime A10 or any other `armhf` where you would be able to target and load the
module.

# Tested platforms

- [Raspberry PI B+ v1.2](https://www.raspberrypi.org/products/raspberry-pi-1-model-b-plus/)
- [Olimex Olimexino Lime A10](https://www.olimex.com/Products/OLinuXino/A10/A10-OLinuXino-LIME-n4GB/open-source-hardware)

# Software requirements

I've only tested *cross compilation* from my desktop machine, Ubuntu or Fedora
based. Theoretically, *native compilation* should work, but you should tweak
the main `Makefile` or run `make CHOST=`.

For *cross compilation* you'll have to provision your desktop machine with this:

1. The GNU/Linux kernel headers or full kernel source for your target (including
   `Module.symvers` file). Be careful with the `scripts` folder. This folder
   usually contains programs compiled for the target, not for the host. If you
   are *cross compiling* you'll have to recompile some of these programs to be
   able to make the kernel module. You can use my [prepartion script](https://gitlab.com/robarago/linuxcc-prepare)
   if you don't want to download and compile all the kernel sources to be able
   to cross-compile.

2. GNU Arm toolchain (i.e. [Linaro](https://www.linaro.org/downloads/),
   [GNU A-profile architecture](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a/downloads) toolchain, etc.)
   Any version >= 4.9.4 should be OK.

# Compile

First, clone this repository ;-)

Then you'd have to make your `arm-linux-gnueabihf` cross compilation toolchain
visible and usable. Maybe it would suffice to put it in your `PATH` like so:

    export PATH=$PATH:/path/to/arm-linux-gnueabihf/tools/bin

Check your toolchain documentation to see where the compiler and friends are
located. If you could reach your compiler, then the toolchain is ready. Check
this, i.e.:

    arm-linux-gnueabihf-gcc --version

Then, edit provided `module/Makefile` and create a new SRC-`yourtarget` var,
pointing to the location of your Linux headers or source (where the file
`Module.symvers` and `Makefile` are located). Additionally, you can change the
default target from `zapp` to `yourtarget`. Now, you'll be able to compile
everything:

    make

If everything goes well, you'll see the module `module/max7219.ko` and the
example programs (i.e. `examples/sendtext/sendtext`). Now it's time to install
these in your target.

    cd module && make install-<yourtarget>

Of course, this will work if your target machine has an `sshd` service running,
you can authenticate successfully with a password or your public certificate
is installed in the target machine, what I would strongly recomend you.

# Testing

## Hardware

Check your target documentation to see the exact pins (or headers) you should
use to connect your hardware, since it will vary a lot between different
platforms. I'll give here an example but your pinout and buses would
certainly be different.

### Olimexino Lime A10

I've not tested `spi0`, but `spi1` requires this pinout:
- Physical pin 272, named PI16 with function SPI1_CS0 on header pin 40 of GPIO2
  connected to the LOAD or /CS pin of the MAX7219
- Physical pin 273, named PI17 with function SPI1_CLK on header pin 38 of GPIO2
  connected to the CLK pin of the MAX7219
- Physical pin 274, named PI18 with function SPI1_MOSI on header pin 36 of GPIO2
  connected to the DIN pin of the MAX7219

Also, you'll need to connect the GND and VCC pin to. i.e.
- Pin number 2, named GND on GPIO2 header connected to GND on the MAX7219
- Pin number 1, named 5V on GPIO2 header connected to V+ on the MAX7219

Of course, when we say *on the MAX7219* we mean the controller of the first
matrix of array chain, since all the rest should be chained together to this.

### Raspberry Pi B+ (v1.2)

Although not tested, this pinout is common to several Raspberry Pi platforms.
AFAIK, RPi 1 to 4 have compatible 40 pin headers with the same pinout for `spi1`.

`spi1` requires this pinout:
- Physical pin 12, named GPIO18 with function SPI1_CE0
  connected to the LOAD or /CS pin of the MAX7219
- Physical pin 40, named GPIO21 with function SPI1_SCLK
  connected to the CLK pin of the MAX7219
- Physical pin 38, named GPIO20 with function SPI1_MOSI
  connected to the DOUT pin of the MAX7219

Also, you'll need to connect the GND and VCC pin to. i.e.
- Pin number 6, named *Ground* connected to GND on the MAX7219
- Pin number 2 or 4, named *5V power* connected to V+ on the MAX7219

![Raspberry Pi3 - MAX7219 Pinout](rpi-spi-pinout.png)

## Software

To use this module, you'll have to insert it in the running kernel. This is
called "out-of-tree" inserting and it *taints the kernel*. Although it sounds
a little intimidating, it won't break your hardware or kill your cat but, while
you don't have any kernel stack dump, hang or kernel panic, you'd be safe.

Please, run this command to see the parameters of the kernel module:

    modinfo max7219.ko

This will tell you the exact parameters you can use by inserting the module in
your system. The most important parameter is the `bus`. Just run this command:

    ls /sys/class/spi_master

To see the buses you have available to connect the `max7219` slave. If you don't
see any, check the section [OS HW Configuration](OS HW Configuration).

If you see at least one entry (i.e. `spi0`), then you can use this bus to connect
the hardware.

# OS HW Configuration

No, SPI buses are not enabled by default in every platform, so you'll have to
enable it by your own means. If this scares your, don't worry, me too. But at
the end of the day, you'll see how easy is it is and you have learned something
new (if you don't know it yet).

## Olimexino Lime A10

In this platform, I opted to recompile de *device tree* to include support for
the `spi1` master bus. In general lines, I decompiled, edited and changed the
DTS file and then recompiled it again with new hardware support.

To do this, you need the `dtc` (*Device Tree Compiler*). Hopefully, you'll have
this tool in your usual repositories or included in your kernel source or
kernel headers.

First, gather the DTB which should be included in the `` /boot/dtbs/`uname -r` ``
folder of your target. Then decompile to DTS:

    dtc -I dtb -O dts sun4i-a10-olinuxino-lime.dtb -o lime.dts

Now, edit the file and simply change in these lines the word `disabled` by
`okay`:

     spi@1c06000 {
      compatible = "allwinner,sun4i-a10-spi";
      reg = <0x1c06000 0x1000>;
      interrupts = <0xb>;
      clocks = <0x2 0x2d 0x2 0x71>;
      clock-names = "ahb", "mod";
      dmas = <0x8 0x1 0x9 0x8 0x1 0x8>;
      dma-names = "rx", "tx";
      pinctrl-names = "default";
      pinctrl-0 = <0x9 0xa>;
      status = "disabled";
      #address-cells = <0x1>;
      #size-cells = <0x0>;
      phandle = <0x3c>;
    };

Then, compile the DTS to DTB, ignoring the warnings:

    dtc -I dts -O dtb lime.dts -o sun4i-a10-olinuxino-lime.dtb

And put the resulting file in the `` /boot/dtbs/`uname -r` `` folder. You would want
to rename previous `dtb` to be able to revert the changes if needed. Reboot and
check if the new `spi1` bus is now in `/sys/class/spi_master`.

## Raspberry Pi B+ (v1.2)

A little simpler. Just locate your `/boot/config.txt` file and add this line
(i.e.) after the commented line `#dtparam=spi=on`:

    dtoverlay=spi1-1cs,cs0_spidev=disabled

Don't uncomment the line `#dtparam=spi=on`, since this should prevent the module
to be able to use the `chipselect 0` (`spi=on` loads kernel module `spidev` and
this should lock the `chipselect 0`).

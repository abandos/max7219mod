#!/usr/bin/env python3
import sys
import struct

def reverse(n):
  return struct.pack('B', int('{0:08b}'.format(n)[::-1], 2))

if __name__ == '__main__':
  with open('output.raw', 'wb') as w:
    with open(sys.argv[1], 'rb') as f:
      for n in f.read():
        w.write(reverse(n))

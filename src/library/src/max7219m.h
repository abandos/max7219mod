/**
 * max7219m.h
 * robarago@gmail.com
 * April, 2020
 * max7219mod kernel module helper library header
 */

#ifndef __MAX7219_H__
#define __MAX7219_H__

#include <stddef.h>

// Device
void max7219mod_open(void);
void max7219mod_send_data(unsigned char* data, size_t length);
void max7219mod_set_delay(unsigned int msdelay);
void max7219mod_set_scroll_mode(unsigned int scroll_mode);
void max7219mod_set_intensity(unsigned int intensity);
void max7219mod_set_anim_mode(unsigned int anim_mode);
void max7219mod_close(void);
// Text
void max7219mod_load_font(const char* fontfile);
void max7219mod_send_string(unsigned char* string);

#endif /* __MAX7219_H__ */

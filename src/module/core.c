/*
 * max7219.c
 * robarago@gmail.com 2020
 * ARMv7 driver module for a MAX7219 based 8x8 matrix array
 */
#include "max7219.h"
#include <linux/module.h>
#include <linux/moduleparam.h>

// Macros
#define TRY(c, m, ...) if(c) { printk(m, ##__VA_ARGS__); return -ENODEV; }

////////////////
// Module params

// Scroll and anim param: not module param, but used as default
unsigned long msdelay = 0U; // Fast (don't call if msdelay == 0) !

// Number of modules in the array
int bus = 1;
module_param(bus, int, 0660);
MODULE_PARM_DESC(bus, "SPI bus master to use (check /sys/class/spi_master)");

// Number of modules in the array
int array_count = 5;
module_param(array_count, int, 0660);
MODULE_PARM_DESC(array_count, "Number of modules in the module array (1..8)");

// Intensity of the matrix display
int intensity = 1;
module_param(intensity, int, 0660);
MODULE_PARM_DESC(intensity, "Default intensity of the matrix array (0..15)");

// Test pattern value
int test_value = 0;
module_param(test_value, int, 0660);
MODULE_PARM_DESC(test_value, "Initial value (0..255) of the initial test pattern");

// Default scroll mode
int scroll_mode = 1;
module_param(scroll_mode, int, 0660);
MODULE_PARM_DESC(scroll_mode, "0: no scroll, 1: left -> right, 2 bottom -> top, 3 top -> bottom");

int init_module(void) {
	// Check parameters range
	TRY(bus < 0 || bus > 4,
		MALERT "SPI bus number is not <= 4 - given %d\n",
		bus);
	TRY(intensity < 0 || intensity > 15,
		MALERT "Default intensity value is wrong (range 0..15) - given %d\n",
		intensity);
	TRY(array_count < 1 || array_count > 8,
		MALERT "Number of led modules in the array (1..8) - given %d\n",
		array_count);
	TRY(test_value < 0 || test_value > 255,
		MALERT "Test value pattern not in range (0..255) - given %d\n",
		test_value);
	TRY(scroll_mode < 0 || scroll_mode > 3,
		"Invalid scroll mode (0..3) - given %d\n",
		scroll_mode);

	// Initialize (register) slave
	if(0 > initializeArray(bus)) return -ENODEV;

	// Create chardev, mutex and workqueue to serve for user space calls
	if(createChardev()) {
		shutdownArray(); // Also destroys slave dev
		return -ENODEV;
	}

	// Initialize data memory block for rendering
	initializeTasksData();

	printk(MINFO "Array module loaded\n");
	return 0;
}

void cleanup_module(void) {
	freeTasksData();
	destroyChardev();
	shutdownArray(); // Also destroys slave dev
	printk(MINFO "Array driver unloaded\n");
}

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Roberto Aragón <robarago@gmail.com>");
MODULE_DESCRIPTION("ARMv7 driver module for a MAX7219 based 8x8 matrix array");

/*
 * tasks.c
 * robarago@gmail.com 2020
 * ARMv7 driver module for a MAX7219 matrix array - tasks for rendering
 */
#include <linux/delay.h>
#include "max7219.h"

extern int array_count;
extern int intensity;

#define	AM_NO_ANIM     0
#define	AM_0TON        1
#define	AM_NTO0        2
#define	AM_0TON_INOUT  3
#define	AM_NTO0_INOUT  4

// Macros
#define TRY(c, m, ...) if(c) { printk(m, ##__VA_ARGS__); return -ENODEV; }

// Array frontend
static struct {
	uint8_t* m; // One extra byte for the double buffered module
} abuf[8];

void initializeTasksData(void) {
	unsigned int i;
	// Alloc space for the internal buffer (8 rows x array_count)
	for(i = 0; i < 8; i++)
		abuf[i].m = kmalloc_array(array_count + 1, sizeof(uint8_t),
			GFP_KERNEL | __GFP_ZERO);
}

void freeTasksData(void) {
	unsigned int i;
	for(i = 0; i < 8; i++)
		kfree(abuf[i].m);
}

void scrollNO(struct work_struct *work) {
	size_t p = 0, l;
	register int j, k, from, to;
	max7219_work_t* w = (max7219_work_t*)work;

	// Fixed intensity - change exactly before running task and only if changed
	if(w->anim_mode == AM_NO_ANIM && w->intensity != intensity) {
		intensity = w->intensity;
		setArrayIntensity(intensity);
	}
	while(p < w->len) {
		for(j = 0; j < 8; j++) {
			for(k = 0; k <= array_count; k++) {
				l = p + (k * 8) + j;
				abuf[j].m[k] = l >= w->len ? 0 : w->data[p + (k * 8) + j];
			}
			sendOneRow(j + 1, abuf[j].m, array_count);
		}
		// If animated text, cycle through modes
		if(w->anim_mode != AM_NO_ANIM) {
			from = w->anim_mode == AM_0TON || w->anim_mode == AM_0TON_INOUT ? 0 : w->intensity;
			to = w->anim_mode == AM_0TON || w->anim_mode == AM_0TON_INOUT ? w->intensity : 0;
			k = from > to ? -1 : 1;
			printk(MDEBUG "Anim 1: %d -> %d (%d)\n", from, to, k);
			for(j = from; j != to; j += k) {
				setArrayIntensity(j);
				msleep(w->msdelay);
			}
			if(w->anim_mode == AM_0TON_INOUT || w->anim_mode == AM_NTO0_INOUT) {
				k = from;
				from = to;
				to = k;
				k = from > to ? -1 : 1;
				printk(MDEBUG "Anim 2: %d -> %d (%d)\n", from, to, k);
				for(j = from; j != to; j += k) {
					setArrayIntensity(j);
					msleep(w->msdelay);
				}
			}
		}
		// forward <array_count> * 8 chars (even if 1 char is only left)
		p += 8 * array_count;
		// Delay between chunks
		if(w->msdelay) msleep(w->msdelay * array_count);
	}
	// Free work data
	kfree(w->data);
	kfree(w);
}

// Scroll buffer contents from left to right
void scrollLR(struct work_struct *work) {
	size_t p = 0;
	register uint8_t j, k;
	uint8_t shift, n = array_count;
	max7219_work_t* w = (max7219_work_t*)work;

	// Change intensity exactly before running task and only if changes
	if(w->intensity != intensity) {
		intensity = w->intensity;
		setArrayIntensity(intensity);
	}
	while(p < w->len || n > 0) {
		// fill only double buffered char
		if(p + 7 <= w->len) {
			for(j = 0; j < 8; j++)
				abuf[j].m[array_count] = w->data[p + j];
			p += 8;
		}
		else n--;
		// Shift left 8 times after reading next char
		for(shift = 0; shift < 8; shift++) {
			// For every module (plus hidden mod for double buffered bitmap)...
			for(k = 0; k <= array_count; k++) {
				// Shift every row one bit left taking only first bit of the buffered char
				for(j = 0; j < 8; j++)
					abuf[j].m[k] = (abuf[j].m[k] << 1) |
						(k == array_count ? 0 : abuf[j].m[k + 1] >> 7);
			}
			// All the buffer is shifted, now send row by row
			for(j = 0; j < 8; j++)
				sendOneRow(j + 1, abuf[j].m, array_count);
			if(w->msdelay) msleep(w->msdelay);
		}
	}
	// Free work data
	kfree(w->data);
	kfree(w);
}

// Scroll buffer contents from bottom to top
void scrollBT(struct work_struct *work) {
	size_t p = 0, l;
	register uint8_t k;
	int shift, j;
	max7219_work_t* w = (max7219_work_t*)work;

	// Change intensity exactly before running task and only if changes
	if(w->intensity != intensity) {
		intensity = w->intensity;
		setArrayIntensity(intensity);
	}
	while(p < w->len) {
		// Shift bottom 8 times and then move buffer pointer N bytes
		for(shift = 0; shift < 8; shift++) {
			// For every module
			for(k = 0; k <= array_count; k++) {
				for(j = 0; j < 7; j++)
					abuf[j].m[k] = abuf[j + 1].m[k];
				l = p + (k * 8) + shift;
				abuf[7].m[k] = l >= w->len ? 0 : w->data[l];
			}
			// All the buffer is shifted, now - send row by row
			for(j = 0; j < 8; j++)
				sendOneRow(j + 1, abuf[j].m, array_count);
			if(w->msdelay) msleep(w->msdelay);
		}
		// forward <array_count> * 8 chars (even if 1 char is only left)
		p += 8 * array_count;
		if(w->msdelay) msleep(w->msdelay * array_count);
	}
	// Free work data
	kfree(w->data);
	kfree(w);
}

// Scroll buffer contents from top to bottom
void scrollTB(struct work_struct *work) {
	size_t p = 0, l;
	register uint8_t k;
	int shift, j;
	max7219_work_t* w = (max7219_work_t*)work;

	// Change intensity exactly before running task and only if changes
	if(w->intensity != intensity) {
		intensity = w->intensity;
		setArrayIntensity(intensity);
	}
	while(p < w->len) {
		// Shift bottom 8 times and then move buffer pointer N bytes
		for(shift = 7; shift >= 0; shift--) {
			// For every module
			for(k = 0; k <= array_count; k++) {
				for(j = 7; j > 0; j--)
					abuf[j].m[k] = abuf[j - 1].m[k];
				l = p + (k * 8) + shift;
				abuf[0].m[k] = l >= w->len ? 0 : w->data[l];
			}
			// All the buffer is shifted, now - send row by row
			for(j = 0; j < 8; j++)
				sendOneRow(j + 1, abuf[j].m, array_count);
			if(w->msdelay) msleep(w->msdelay);
		}
		// forward <array_count> * 8 chars (even if 1 char is only left)
		p += 8 * array_count;
		if(w->msdelay) msleep(w->msdelay * array_count);
	}
	// Free work data
	kfree(w->data);
	kfree(w);
}

void (*max7219_fn[])(struct work_struct*) = {
	scrollNO,
	scrollLR,
	scrollBT,
	scrollTB
};

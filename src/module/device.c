/*
 * device.c
 * robarago@gmail.com 2020
 * ARMv7 driver module for a MAX7219 matrix array - userspace device funcs
 */
#include "max7219.h"
#include <linux/fs.h>
#include <linux/device.h>

struct workqueue_struct* max7219_wq;

#define DEV_NAME       "max7219m"
#define CLASS_NAME     "max72xx_spi"

// Module and scroll params
extern unsigned long msdelay;
extern int intensity;
extern int scroll_mode;
extern void (*max7219_fn[])(struct work_struct*);

#define BUF_MAX_SIZE 4096

// Macros
#define TRY(c, m, ...) if(c) { printk(m, ##__VA_ARGS__); return -ENODEV; }

// Locking the device
static DEFINE_MUTEX(max7219_mtx);
// Array backend
static char     max7219_buf[BUF_MAX_SIZE];
static size_t   max7219_buf_len;
static max7219_work_t* max7219_work;
// chardev endpoint
static unsigned int IOCTL_SET_MSDELAY;
static unsigned int IOCTL_SET_SCROLL_MODE;
static unsigned int IOCTL_SET_INTENSITY;
static unsigned int IOCTL_SET_ANIM_MODE;
static int      major;
static struct   class*  chardevClass;
static struct   device* chardevDevice;

static long dev_ioctl(struct file *fp, unsigned int in, unsigned long ip) {
	if(IOCTL_SET_MSDELAY == in) {
		TRY(ip < 0 || ip > 1000, "Invalid msdelay value (0..1000)");
		max7219_work->msdelay = (int)ip;
	} else if(IOCTL_SET_SCROLL_MODE == in) {
		TRY(ip < 0 || ip > 3, "Invalid scroll mode (0..3)");
		max7219_work->scroll_mode = (int)ip;
	} else if(IOCTL_SET_INTENSITY == in) {
		TRY(ip < 0 || ip > 15, "Invalid intensity value (0..15)");
		max7219_work->intensity = (int)ip;
	} else if(IOCTL_SET_ANIM_MODE == in) {
		TRY(ip < 0 || ip > 7, "Invalid animation mode mask (0..7)");
		max7219_work->anim_mode = (int)ip;
	} else printk(MALERT "Invalid `ioctl' - check your data\n");
	return 0;
}

static int dev_open(struct inode *in, struct file *fp) {
	TRY(!mutex_trylock(&max7219_mtx),
		MALERT "Device is busy - please try later\n");
	// Allocate async task
	max7219_work = (max7219_work_t *)kmalloc(sizeof(max7219_work_t), GFP_KERNEL);
	if(max7219_work) {
		// Set default values
		max7219_work->msdelay = msdelay;
		max7219_work->intensity = intensity;
		max7219_work->scroll_mode = scroll_mode;
		max7219_work->anim_mode = 0;
	} else {
  	printk(MALERT "ERROR while allocating workqueue data\n");
		mutex_unlock(&max7219_mtx);
		return -1;
	}
  return 0;
}

static int dev_release(struct inode *in, struct file *fp) {
	//Start async task only if max7219_work is allocated
	if(max7219_work) {
		INIT_WORK((struct work_struct *)max7219_work,
			max7219_fn[max7219_work->scroll_mode]);
		max7219_work->len = max7219_buf_len;
		max7219_work->data = (u8*)kmalloc(sizeof(u8) * max7219_buf_len, GFP_KERNEL);
		memcpy(max7219_work->data, max7219_buf, max7219_buf_len);
  	printk(MDEBUG "stats: len=%d dir=%d delay=%d int=%d anim=%d\n",
			max7219_work->len, max7219_work->scroll_mode, max7219_work->msdelay,
			max7219_work->intensity, max7219_work->anim_mode);
		queue_work(max7219_wq, (struct work_struct*)max7219_work);
		// Don't release the mutex until the message is fully written
		mutex_unlock(&max7219_mtx);
	}
  return 0;
}

static ssize_t dev_write(struct file *fp, const char *buf, size_t len, loff_t *off) {
	ssize_t ret;
	ret = simple_write_to_buffer(max7219_buf, sizeof(max7219_buf), off, buf, len);
	if(ret < 0) return ret;
	max7219_buf_len = ret;
	return ret;
}

// register chardev functions
static struct file_operations max7219_fops = {
	.open = dev_open,
	.write = dev_write,
	.release = dev_release,
	.unlocked_ioctl = dev_ioctl,
};

// Create chardev
int createChardev(void) {
	major = register_chrdev(0, DEV_NAME, &max7219_fops);
	TRY(!major < 0, MERROR "Call to register_chrdev failed\n");
	IOCTL_SET_MSDELAY = _IOR(major, 0, int);
	IOCTL_SET_SCROLL_MODE = _IOR(major, 1, int);
	IOCTL_SET_INTENSITY = _IOR(major, 2, int);
	IOCTL_SET_ANIM_MODE = _IOR(major, 3, int);

	chardevClass = class_create(THIS_MODULE, CLASS_NAME);
	if(IS_ERR(chardevClass)) {
		unregister_chrdev(major, DEV_NAME);
		printk(MERROR "Call to class_create failed\n");
		return -ENODEV;
	}

	chardevDevice = device_create(chardevClass, NULL, MKDEV(major, 0), NULL,
		DEV_NAME);
	if(IS_ERR(chardevDevice)) {
		unregister_chrdev(major, DEV_NAME);
		class_destroy(chardevClass);
		printk(MERROR "Call to device_create failed\n");
		return -ENODEV;
	}

	// Allow to lock the device interface
	mutex_init(&max7219_mtx);

	// Create work queue to allow async queued transfers
	max7219_wq = create_workqueue("max7219m_queue");
	if(max7219_wq == NULL) {
		printk(MERROR "Can't create workqueue\n");
		return -ENODEV;
	}

	printk(MINFO "/dev/%s ready\n", DEV_NAME);

	return 0;
}

void destroyChardev(void) {
	flush_workqueue(max7219_wq);
	destroy_workqueue(max7219_wq);
	device_destroy(chardevClass, MKDEV(major, 0));
	class_destroy(chardevClass);
	unregister_chrdev(major, DEV_NAME);
	mutex_destroy(&max7219_mtx);
}

/*
 * max7219.h
 * robarago@gmail.com 2020
 * ARMv7 driver module for a MAX7219 matrix array - Common definitions and data
 */
#ifndef __MAX7219_H__
#define __MAX7219_H__

#include <linux/slab.h>

// Logging facility constant
#define M              "MAX7219: "
#define MERROR         KERN_ERR   M
#define MALERT         KERN_ALERT M
#define MWARN          KERN_WARN  M
#define MINFO          KERN_INFO  M
#define MDEBUG         KERN_DEBUG M

// Max number of modules in the array chain
#define MAX_ARRAY_COUNT 8

#define DECODE_MODE     0x09
#define INTENSITY       0x0a
#define SCAN_LIMIT      0x0b
#define DISPLAY_TEST    0x0f
#define SHUTDOWN        0x0c

typedef struct {
	struct work_struct work;
	u8* data;
	size_t len;
	unsigned int intensity;
	unsigned int scroll_mode;
	unsigned int anim_mode;
	unsigned int msdelay;
} max7219_work_t;

// spi.c
void setArrayIntensity(unsigned int intensity);
void shutdownArray(void);
void sendOneRow(unsigned char reg, unsigned char* cols, size_t len);
int initializeArray(int bus);

// device.c
int createChardev(void);
void destroyChardev(void);

// tasks.c
void initializeTasksData(void);
void freeTasksData(void);

#endif // __MAX7219_H__
